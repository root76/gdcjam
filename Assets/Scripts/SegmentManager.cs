﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SegmentManager : MonoBehaviour {

	public Segment[] SegmentPrefabs;
    List<Segment>[] entryPointsToSegments = new List<Segment>[] {
        new List<Segment>(), 
        new List<Segment>(), 
        new List<Segment>(), 
    };

    [Range(0, 50)]
    public float ScrollSpeed = 1;
    [Range(0, 5)]
    public float ScrollAcceleration = 0.1f;
    [Range(0, 50)]
	public float MaxScrollSpeed = 10;

	public List<Segment> Segments = new List<Segment>();
    List<Segment> segmentsToKill = new List<Segment>();
    [Range(0, 20)]
    public float SegmentGap = 10f;
    [Range(-20, 20)]
    public float SegmentPositionKillLimit = -6.5f;
    [Range(-20, 20)]
	public float SegmentPositionSpawnLimit = 6.5f;

    void Start() {
        for (int i = 0; i < SegmentPrefabs.Length; i++) {
            entryPointsToSegments[SegmentPrefabs[i].EntryHeight].Add(SegmentPrefabs[i]);
        }
        for (int i = 0; i < entryPointsToSegments.Length; i++) {
            if (entryPointsToSegments[i].Count == 0)
                Debug.LogError("No segments with entry point " + i + "!!");
        }
	}

	void Update() {
        var rightmostSegmentPos = Segments.Count > 0 ? Segments[0].transform.localPosition : new Vector3(0, 0);
        var rightmostExitPoint = Segments.Count > 0 ? Segments[0].ExitHeight : 0;

		foreach (var segment in Segments) {
			segment.transform.Translate(-ScrollSpeed * Time.deltaTime, 0, 0);
		    if (segment.transform.localPosition.x > rightmostSegmentPos.x) {
		        rightmostSegmentPos = segment.transform.localPosition;
		        rightmostExitPoint = segment.ExitHeight;
		    }

		    if (segment.transform.localPosition.x < SegmentPositionKillLimit)
				segmentsToKill.Add(segment);
		}

        if (rightmostSegmentPos.x <= SegmentPositionSpawnLimit) {
		    var segmentList = entryPointsToSegments[rightmostExitPoint];
            GameObject newSegmentObj = (GameObject)Instantiate(segmentList[Mathf.FloorToInt(Random.value * segmentList.Count)].gameObject);
			newSegmentObj.transform.parent = transform;
			newSegmentObj.transform.localPosition = new Vector3(rightmostSegmentPos.x + SegmentGap, rightmostSegmentPos.y, rightmostSegmentPos.z);
            Segments.Add(newSegmentObj.GetComponent<Segment>());
		}

		ScrollSpeed += ScrollAcceleration * Time.deltaTime;
		if (ScrollSpeed >= MaxScrollSpeed)
			ScrollSpeed = MaxScrollSpeed;

		foreach (var segment in segmentsToKill) {
			Segments.Remove(segment);
			Destroy(segment.gameObject);
		}
		segmentsToKill.Clear();
	}

    void OnDrawGizmos() {
        var pos = SegmentPositionKillLimit;
        while (pos < SegmentPositionSpawnLimit) {
            Gizmos.DrawLine(new Vector3(transform.position.x + pos, transform.position.y + 5.4f, 0), new Vector3(transform.position.x + pos, transform.position.y - 5.4f, 0));
            pos += SegmentGap;
        }
    }
}
