﻿using UnityEngine;
using System.Collections;

public class Globals : MonoBehaviour {

    void Awake() {
        if (FindObjectOfType<Globals>() != this) {
            Destroy(gameObject);
        } else {
            DontDestroyOnLoad(gameObject);
        }
    }
}
