﻿using UnityEngine;
using System.Collections;

public class Segment : MonoBehaviour {

    [Range(1, 2)]
    public int JumpAmount = 1;
    [Range(0, 2)]
    public int EntryHeight = 0;
    [Range(0, 2)]
    public int ExitHeight = 0;
}
