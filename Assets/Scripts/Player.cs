﻿using System;
using System.Runtime.Serialization.Formatters;
using UnityEditor;
using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    [Range(-5, 5)]
    public float[] LaneHeights = {-3.6f, 0f, 3.6f};
    [Range(0, 2)]
    public int CurrentLane = 1;
    int oldLane = 1;

    public GameObject BulletPrefab;

    public AnimationCurve MovementCurve;
    [Range(0, 1)]
    public float MoveTime = 1f;
    float moveTime = float.PositiveInfinity;
    float moveFromY;
    float moveToY;

    public AnimationCurve SpeedToShakeStrength;

    SegmentManager segmentManager;
    Shaker shaker;

	void Start() {
	    segmentManager = FindObjectOfType<SegmentManager>();
	    shaker = GetComponent<Shaker>();
	    transform.position = transform.position.withY(LaneHeights[CurrentLane]);
	}
	
	void FixedUpdate() {
        var device = InControl.InputManager.ActiveDevice;
        // handle changing lanes
	    {
	        Segment currentSegment = null;
	        for (int i = 0; i < segmentManager.Segments.Count; i++) {
	            var segment = segmentManager.Segments[i];
                if (segment.transform.position.x <= (segmentManager.SegmentGap / 2) && segment.transform.position.x >= -(segmentManager.SegmentGap / 2)) {
                    currentSegment = segment;
                    break;
                }
	        }
	        var jumpAmount = currentSegment != null ? currentSegment.JumpAmount : 1;

            if (device.Direction.Up.WasPressed && CurrentLane < (LaneHeights.Length - 1)) {
                CurrentLane = (CurrentLane + jumpAmount + LaneHeights.Length) % LaneHeights.Length;
            } else if (device.Direction.Down.WasPressed && CurrentLane > 0) {
                CurrentLane = (CurrentLane - jumpAmount + LaneHeights.Length) % LaneHeights.Length;
            }
            if (oldLane != CurrentLane) {
                MoveTo(LaneHeights[CurrentLane]);
            }
            oldLane = CurrentLane;
        }
        // handle shooting
	    {
	        if (device.Action1.WasPressed) {
	            var newBulletObj = (GameObject) Instantiate(BulletPrefab, transform.position.plusZ(0.01f), transform.rotation);
	        }
        }
        // tween Y position
        {
            if (moveTime < MoveTime) {
                moveTime += Time.deltaTime;
                var interp = MovementCurve.Evaluate(moveTime / MoveTime);
                var newY = Mathf.Lerp(moveFromY, moveToY, interp);
                transform.position = transform.position.withY(newY);
            }
        }
        // shake player graphics based on how fast segment manager is scrolling
        {
            var shakeStrength = SpeedToShakeStrength.Evaluate(segmentManager.ScrollSpeed);
            shaker.Strength = shakeStrength;
        }
	}

    public void MoveTo(float yPosition) {
        moveTime = 0;
        moveFromY = transform.position.y;
        moveToY = yPosition;
    }

    void OnDrawGizmos() {
        for (int i = 0; i < LaneHeights.Length; i++) {
            Gizmos.DrawWireSphere(new Vector3(transform.position.x, LaneHeights[i], 0), 0.2f);
        }
    }
}
