﻿using UnityEngine;
using System.Collections;

public class CameraOffset : MonoBehaviour {

    public AnimationCurve SpeedToXOffset;
    SegmentManager segmentManager;

    void Start() {
        segmentManager = FindObjectOfType<SegmentManager>();
	}
	
	void Update() {
        // offset camera X based on how fast segment manager is scrolling
        {
            var offset = SpeedToXOffset.Evaluate(segmentManager.ScrollSpeed);
            transform.position = transform.position.withX(offset);
        }
	}
}
